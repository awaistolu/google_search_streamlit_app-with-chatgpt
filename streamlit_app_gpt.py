import streamlit as st
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time
from bs4 import BeautifulSoup
from UnlimitedGPT import ChatGPT

def selenium_app(user_input, q_input):
    session_token = 'eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2R0NNIn0..-Au_0dO2taxBm353.dgTHUjnOMEb2MM5m6B-Uad_bypI8fsI300oA2y1wf3i2p7oNuobYWXvj0_vWCKm2hDzE_O91sma-ovjwTTS9FIBXSV7n6e7rBLbImWtNgmepWjzgefyQU3axzLJ3RP3RsLWF2m075EBm4IXE544xFQ-FZfySJOwlZBaSRk_EGm3jh9cplhClzEDWe-xAMcA5r40Ob-EGyg14KwyvQYgi-MpVQK9k3zAa2BuRvFRwGGwXf55VwSephhFCWcpZxRRLj93wci5K5q_kccrZJYgRIbnmmbycqvw75QWYHCSSryLOEMGUecrlEgVWTFBFhAnLASX2WD8j6oD2ap1fzO602CBXGRkB3k7aCyW2_Mig-0NX9jMV9eKgAIXsVff2vHSU71LzpdOwzmuvIxMhiF1PPvr8qynlgnWHosWVHOrrPsnTki8BgKPAWQXU2QWZR-XUHbw-3aI7FmCYQYvUvf85Tqnzt8deAqYj__enICO7Fh_N4nOGvMbkZIReeSr-5vfcCADv9XlIOKXa1hYE3YOdOu0ctQeiFQzOAaWFdPzS8qQD9VHdQ9-wNXlCFh0l1o-magp6ZJTCV2aG2m70PCnjtAla1A527MZK7lbyzDrqPw8jeLvanscpizuwmd8WLm9oop44mbOawGYL38dwT8dQjqe8na1sLErTsVnaZ05np4l2OsBlgcKnFRI-K3ojIDAVQc7QDWrcv9LuKrxb0bSTB7yJ38WsY1BUOWSX5twjO9ywKrHUDGy8j0J5E_nRGRE7s5uktYQuCoF22jJumNo10FNwgyewLT45roN1xKW7y8MgeoPJiveYdGB9A0syoBrIazF65vi0Seq-Fb3qqGdDSdoewRsCstJcNqVfQ5l7OCi1LgG9UFjUbAgNv5g1MXwStRSyJtv2tIMwiqJ-STY31QN07kHiQeHEoU6Ke-NBjxZUJMGu6RJ8121F-FmTahtKo5ghnUm-1Kl0nkVgx1DUsZSFFI1zT_xxl-o3w4rlp4nwYm3rcrxZa1E6hy5Xf1ScytWzMzRywS-2mIeoYdhGFZcOnrAAjdWqQ2JHVFHsX26Z69ih5IWDDVxOz7HB0e9ixe_LyMpMum3hoqJi3TO30ZsCRnmggZOCmC8HxUYrLErcP54q6woQk2SnsoBY98pJgh9ljwdUouJrQJx6WiYdsBC5PnrLe5FFOHQGK3E2rp-hnsHMJf4aayYsot7VfN7tffUB-eMsVM3XkrWgx_urtTb23hzXn8kdR-MfIi8rtgfEDxs3UpvHtAEJZ4693GL_GlEzXQzWk8IAU8qiQJJDhDmEmqrviNrqZ__mMWqEIZz7rV8YKCpns9LZdxOUCYnkMJd8z1VkL8XDMPeO3tTSw2_uoLdunwWmq3Us8AjfLmFX6LkwQQqM0LMj0lYFBJSYJBGd9XrCRVgO7ntYeUjQQIG6VRAv7bVXmyov09Au8F7YMoowEqGkf2BztkYe2u3Fits3phQbHdXhKB8PyK4mqRzj_rak1MAt5NnuuFie5BI-EM7jTE9cZ5oP4eV5c5hXsda1bhgHv_lGOoXaKH05x2BF8QBKicMN0UFVPgYt3D_l5Qwqkl1sgY_6PLuN4m-G8DlfT5iLYeOpPvSEAecxvU6vdlVMMxJe4KXPscCv9qit1wPxnAmYqbRv8depmbzf6sD1lcXjClWmth9mqa2vW4BHUEEyLFEfv1Cy5tKd1OzUUS2hNPHPD3YxWMCjCZ-qJ2rkFQ7QQ44zzxgHrJJxeOiiefq38ZoN4yk4M_rw7EEdYg7d4HrehKV4qL2k3jG4RWxylk741DGPmHFku5NhcuoQcjt22gRFPYgVzJoASqOuIWtcd6UarIl42AYnwvKQoK1-tJ5QB0Ac8NAwwPqKDm3OezMpaWEgbIzQMXUUt10SIRsBZ96lmGV_pGyBa18-AoKqXr1_WLS7ljmrv5GE5EUmt1ZOWo5HB_UMOW35tujCxRZyqvWixzHRyO8TjidS9RIgi3kTcnCTLpXd3z7JpXsoKRek__9l-aBuO4IY8FKlf--nKZcyRS4RdQCDUmtShC9xSdk_xf9sXQ-3Y6_Y7bNUQRPPTANjhTBkI4lFwtck0U_bAKzRcb0vtH_hSSLReJP-HjVym967dae8Ud75s8GcnYU1zqQ-xybLVl7xJTm5Zf5kR-U-DvcZNQoHwmWniqN1zVrzQG0_4PIKr94h0gkp9qJ5R_mPkcjcsWaDM3BcMlMrBgGsRKr-dv0lPy0vVz6YVD9XQjp0xEI5387S3Rci9Itn6OvBhN3wqlCiSLpYH8f668c0LhXRfekOiL6WvwdkWkThVA5Q9ZLp3VWP9OlOFTpz0t5lyHuWODRiV3SE1nhtbv1vxluolF7149D8qpDu1KUD1JDYLExQW1HWdclXAtBlpE_e8bE5IuvFU8uBmwPErzCoaY-lHWRucn3XT0btMUERFp41zALJhH7TykPGkxJ7IOYavwYFodSDraQ-pMugVhr1sDcwTLBjfuQGe9_Oo_e0n5Non1XxzZKjW_Zt67ZDAz65d2wNPjKnXy_vebEg7Joe4LLE70rrFQ5blvcZSPuvyHc50E8mg_b9GXGmv8sxrjLTsg2NE5bAmDsjqF_LZD1jmyLFIuWrVXGgjhM8ohEWUS0F1vgiNUFmugApAB46SLdruU8l5PpogUlleoeFMMYcEpi0WWnlj-ZwD0lgwg.fWOxSmEKzzTzVKrCGguULQ'
    # Rest of the code
    api = ChatGPT(session_token, conversation_id='')

    chrome_options = Options()
    chrome_options.add_argument('--headless=new')
    driver = webdriver.Chrome(executable_path=r"C:\Users\MuhammadAwais\Downloads\chromedriver_win32 (1)\chromedriver.exe", options=chrome_options)

    # Navigate to Google
    driver.get("https://www.google.com")

    # Find the search box and enter a query
    search_box = driver.find_element(By.NAME, "q")
    search_box.send_keys(user_input)
    search_box.send_keys(Keys.RETURN)

    # Wait for the page to load
    time.sleep(1)

    # Find all the result links and click on them one by one
    result_links = driver.find_elements(By.XPATH, "//div/a/h3")
    data = []
    for link in result_links:
        try:
            link_url = link.get_attribute("href")
            if link_url is None:
                parent_element = link.find_element(By.XPATH, "..")
                link_url = parent_element.get_attribute("href")
            link.click()
            time.sleep(1)
            html = f'''{driver.page_source}'''
            soup = BeautifulSoup(html, 'html.parser')
            text = soup.get_text()
            formatted_text = '\n'.join(line.strip() for line in text.split('\n') if line.strip())
            message = api.send_message(
                f'{q_input} :{formatted_text}'
            )
            answer = message.response
            data.append({"URL": link_url, "Price": answer})
            # Print the question and its corresponding answer
            api.reset_conversation()

            driver.back()
        except:
            continue

    # Close the webdriver
    api.clear_conversations()
    driver.quit()
    return data

def main():
    st.set_page_config(page_title="Selenium Streamlit App", layout="wide", initial_sidebar_state="collapsed")
    st.title("Web Scraping and ChatGPT App")
    st.sidebar.title("Dashboard")
    user_input = st.sidebar.text_input("Enter your Google search")
    q_input = st.sidebar.text_input("Enter your Chatgpt question")
    if st.sidebar.button("Scrape and Chat"):
        with st.spinner("Scraping data..."):
            data_1 = selenium_app(user_input, q_input)
        st.success("Data scraped successfully!")
        st.write(data_1)

if __name__ == "__main__":
    main()

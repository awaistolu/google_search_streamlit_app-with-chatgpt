import flask
from flask import Flask, jsonify, request
from flask_cors import CORS
from UnlimitedGPT import ChatGPT


app = flask.Flask(__name__)
app.config["DEBUG"] = True

CORS(app)

cors = CORS(app, resource={
    r"/*":{
        "origins":"*"
    }
})

import http.client

import json
import ast

session_token = 'eyJhbGciOiJkaXIiLCJlbmMiOiJBMjU2R0NNIn0..wXjDIdGIR63mbL_o.PS7OFpV_Bnirh9SuMobvEjsViyQ3GipkaT2YeQqmIEpL6erSAQPDzhk2rKFf1DKBHNa9_2AOyc9gpL4zbj2Y4uuROFaowliCvoMrrPErsRcCILGuor6IXrL7qU0sjhw8ASGE5n0Y5lYRvpGYvk-4VCLRzSRJ9Da0WN0DuR8zZm83jpiTDLpDWzTOewDnLs8gtv3-OYu1OeNOo5MCT1sqgaxsC6HuS1EyRFFiRcSPC0TEWzt9PsCvlkRGo8cHgMdUVfGDnEI7vm9xQ0YLMT_6AzELud2zZDwuWBJ6WXGawdWreXdjvJFYfh5j8EjgVK3PcW411V6BGNwbYjFYt7ncwsEe27pwf3xU9MysBrk4rQYeQvHT8qcLdk-8Du1TwDC0ouEES0T6MeoODNJb9LuUnvRZfxe_p-AwyxtD-G38Wk7BO4x4J9sbgVAKzgV1XJ9iJ3jlRCNDgJxgrLXqlW7GguEhbRz9M9PaHEJJ_DWKmF2TyLtgNXje3LNuJyO2iVNM3qfC3ZjRrNlrTjL-A4IYnW7x5k2i22fGL3DJ-I_W49Hs_OfE6Wq_LYRfb-6rV14j9Cp4fl4d1mOwZnpO4b7ywIPVb03rGFYKUwNNYhoAKq1wCNxPjsBXuwbJ8CQww5Pegh2zCrjGxiCFcb9I7ArQyMNqKffUh5ozq6oP8BkZzox1EJjU7fTuAW6TSr-DdL8P5z51mYtnETp90tD2WRrqWxtTssp8b5JE5q2xWFB3uks7waKS3dddOmUblyiz2uX_WSCrjqga_kM9uiJXdt86ymXrdkDKG-DNHrrIZqNcKLSbbVgyTJLEqpkHk0pySHFmEx6865AlB4QN0xDqdPgo_ZpaxkfIinWBaMDrbWQKSFTKXBdEok-SCUc8bxE_Maqk4qtF52VnTrKfkNmzV-bk6_Qo6nGtcENalJNRrRLg1Dcb0mi7ENCW0U0i6Mn2ixwKjjVVDQbxtwmX0LbvgN-pfD_Bilk5OmjAB-wr3WtxrXtC-QSwVvtF46WYw1mODxYvRcyzZWQxtBp0b-1CtKenypT1lbHadQmOCgKwkijY1X6v45eGrV8jHt1gezd2UBJizQe4ucKLK2CGMA1lfVE8WrNpaz0kwQfyroKe_iS35R-RLyD9UHkhqqEbpmsaeSKmqfRcNY-LlYMbxeQdkMUdN5iwIwrlqcWCaGQTWHGj0VX0FJE-gNBhb3UVhpbE_Yak4NC82Nbbpuzj8ANzMIYc-HTJCcjCZobsBjkU8t3pspPiGWs77KmP5SOYIyHmhH5Q0hdW_TUofB8Ag0emJAhuAPgbL4i6OesIOku19n8q_tml00m2qk0jB0eqxbgIykzd9j32Gsz8dKk95rNJ5eHclxwbi1FqsygszxpCif5FU7uyIAQA9uj4_Tgy5LKlkRzbdlLl3BmpoDl1QolauPfTmvicp-7UccBsojjqYZ_YiToJYRTQUrhPMVZQzRD0StJY8qwMLJeUO4I9JdFy3fy2wvHDoaZ7ahtpKGGYUI-0ZypgDc7cDZn4F2clIw8-EpPt3KUheLir-zZdWbf6D7_XmuICwSfENvaLzeEBAbCF0n8tLg8tM5cmBD2QNKc1iST-Uw44y7foOdNe70jcJoG-34P9-nIB8BpG59wByuto-1UD14ZN_0DKjDNXdwsJK2PL6dzrJ6DHzelhwCauFoPdImI9evF0tXPm0jCdXNsTnWdHl5Nfrd4totw5TZ1HLhOwkPnDrwhUKdRt4ugx47CB9bQtaRqz23hi5xBBjaMVnZ0aJxn9T3vSLhWU5BvmQRPGN5nbbhUrAi5ucj0EMRcsVTW3ByAUF1iEvSMPVpYjh3-fdzMQMmZETGyZAvRR3DUVbg36JomROz_soeCmznucPhstaWrqiwfqVZ77-zHepuIupdx_0wTGG09n9bz5Qzbj6iEf7Kh23Nvi3Ph6KBUBm0DeRcLSykCduZ7YTEYCTZpqcjRMdaxAvNx0tvxAkQ0jpoIfWLTIUwp8GgXzVyohohDpOPR38DDTzqIdH8-zQ-cA71zyZt79Lt5iotbxuoXs8eCvtd0B_MPmHNHVI16eNmZW_6hA-ANioxCbtl5peh9lfSTg73qMfjwudEcqAP4Xv9XZDgjGAlrca73nY3TpE-CVw6Pcw8335aosIdwvWsZDyufBmKey8WBsC1F6AMVpZpIVhJsDZKiqbEpWtbBKV_UelrejzVmSkDxK5VxdTI82HtnBMSLvbLFh-w1GGicF14hjfbDS9WG4gfGutopE42M5I4PlMzYKP3KIiE5aK0rYk41QuE2o_IgAJuS0wzPcMPID4EnqmJJbW4F4D_Xw4VnDrb08LXDe6TNn5EOai4mhL5daWFO0rFTWIRhDCC5fidZKJ2BOi2T1Kq4vBiiecau4iSskevFH48TOnq9ggg1_yXnLDbnkJxHZX_R6OQFwZXR4tgHNWeAD7YRwMMbc3L3xrVgL78t7emfCWJO6JAsrBPXF-r_9iZEKe9H-VtxNHov7FqQv29nq6HKiHUWfCSimrh9xVBMW8aqIzsRfLwSWWiy8nvPG5y0-TMf1RwwVb6UVkQiDAwg3hfYiY2LttIJoa0IQaksq61Sm9nkAppBmwMwsWER8BD23_MrUy5zkFL2ACTqgIBpQwVLr5hs5hxU0BnesMhcxcr0adKo6280ZhgsP8r2tiQqdlntSmB7r4izvjg.O65uWy6M39IaLG3UBxwW0w'  # `__Secure-next-auth.session-token` cookie from https://chat.openai.com/chat
api = ChatGPT(session_token,conversation_id='')
api.clear_conversations()
@app.route('/chatgpt', methods=['GET'])
def gpt():
    text_data = request.form['data']
    qurey_data = request.form['qurey']
    # print(text_data)
    lines = text_data.splitlines()
    # print(len(lines))
    less_than_500_lines = ''
    # Add lines to the variable until 500 lines are reached or there are no more lines
    for line in lines:
        if len(text_data.splitlines()) < 500:
            less_than_500_lines += line + "\n"
        else:
            break

    message = api.send_message(
        f'give me only price of {qurey_data} :{less_than_500_lines}'
    )
    print(message.response)

    api.reset_conversation() 
    # api.clear_conversations()
    return jsonify({"Price":message.response})


if __name__ == '__main__':
    app.run()
